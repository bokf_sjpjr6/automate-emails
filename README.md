# AWA Notes

* Not working locally bc windows task scheduler changing path, cant find SQL scripts. So use AWA!
* Make sure job has "ENERGY" in it bc Energy Analyst has access to such jobs
* Error Emails for All Tasks
* Automate Customer List
    * Need error email if excel doesnt end up in place with >100 rows
        * Can happen if not connected to VPN at home when it tried to run?
        * If dates are wrong no rows are return as powershell errors, but it doesnt let me know
        * In May 2nd, "Generate MOnthly Customer List" did not run on schedule
            * but it did run correctly when i ran it manually
* Links
    * `http://awadev.bok.com/` 
    * `\\ntprod.pri\awatest\Executables\Dev\ENERGY`

# Automation For EDA (Energy Data Analytics)
Automate Various Processes for Energy Data Analytics including:

- Anomaly checks on EnergyDB Promotion Process
- Generating a Customer List and putting it on a share drive
- Hitting Enverus API for a table, then load it into SQL Database

## Generate Monthly Customer List
### Queries CPDM for Current Month's Energy Customers and Dumps to Excel on Share Drive

Queries CPDM for current month's Energy Customers

Have to pass date formatted as `YYYMM` to query

Create Excel file and put output in sharedrive at:

```{}
\\netapp1\energyengineering\Energy DataMart\Customer Numbers\
```

Title Excel as:

```{}
Customer_Numbers_YYYYMMDD.xlsx
```

where `YYYYMMDD` is the current date

### Schedule
Run on second business day of each month @ 8 AM CST

## WasPromoted is 1 But Not EnergyDB
Queries Energy Data Mart for files where the promotionMaster table indicates that promotion occurred but the file is not found in EnergyDB

Does not flag for files deleted from EnergyDB according to the deletion schedule in which:

- Only the latest file for each Customer Number is kept
- No files older than 13 months are kept

Drops and Creates `StringSplit` function on EnergySTG database used to parse price deck and customer number to determine latest file per Customer Number

Converts returned table to HTML so it can be emailed using function in **Convert Datatable to HTML Function.ps1**

Emails result to: `spatterson@bokf.com` and `EnergyAnalytics@bokf.com`

### Schedule
Run on weekdays at @ 8 AM CST

## WasPromoted Is 0 But in EnergyDB
Queries Energy SQL Data Mart for files where a file is found in EnergyDB, but it has no corresponding record in promotionMaster table with `WasPromoted=1`

Converts returned table to HTML so it can be emailed using function in **Convert Datatable to HTML Function.ps1**

Emails result to: `spatterson@bokf.com` and `EnergyAnalytics@bokf.com`

### Schedule
Run on weekdays at @ 8 AM CST

## PESponsor - With API
### Hit Enverus API for Token, then for table
### Deletes and recreates table in SQL with new data

Sets up request for Enverus API v2 Token, then requests token

Uses token in second request from Enverus API v2 for Company Acreage Table(PE Sponsor)

Converts result to Datatable using function found here: `\Functions\Object To Datatable Function.ps1`

Uses `DBATools` Module to:
1. Drop SQL Table `lu_PESponsor` if it exists
2. Create `lu_PESponsor` and Insert Datatable into it
3. Delete Rows from SQL table Where PESponsor is Null or length=0
4. Correct column types/sizes in SQL table `lu_PESponsor`

### Schedule
Run on second business day of each month @ 8 AM CST
 