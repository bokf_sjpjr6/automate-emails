﻿# Current Filepath
$filepath_home = Get-Location
$filepath_home=$filepath_home.Path
# Filepath of SQL Script
$FilePath_SQL = "$filepath_home\Scripts\WasPromoted Is 0 But in EnergyDB.sql"

# Import-Module SqlServer
# $Datatable=Invoke-DbaQuery -SqlInstance "VRT-SQL-017-WDP" -Query $sqlquery
$SQLServer="VRT-SQL-017-WDP"
$myconectionstring="server='$SQLServer';trusted_connection=false; integrated security='true'"
write-host "Beginning SQL Query"
$Datatable=Invoke-sqlcmd -InputFile $FilePath_SQL -connectionstring $myconectionstring -OutputAs DataTables
write-host "Finished SQL Query"
# Print Table for Debug
#$Datatable | format-table

# Function to Convert Table to HTML
# https://sqljunkieshare.com/2015/12/25/convert-data-table-to-html-table-with-powershell-and-more/
. "$filepath_home\Functions\Convert DataTable to HTML Function.ps1"

# Run Function to Convert Data Table to HTML
$Datatable_HTML = Convert-DatatablHtml -dt $Datatable

# Send Mail IF there are > 0 Rows of table
write-host "Preparing Email"
if ($Datatable.Rows.Count -gt 0) {
    $Outlook = New-Object -ComObject Outlook.Application
    $Mail = $Outlook.CreateItem(0)
    $Mail.To = "spatterson@bokf.com;EnergyAnalytics@bokf.com"
    $Mail.Subject = "WasPromoted Is 0 But in EnergyDB"
    $Mail.HTMLBody = $Datatable_HTML
    $Mail.Send()
}