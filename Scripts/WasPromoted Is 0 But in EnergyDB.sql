WITH BASE AS(
   SELECT p.wasPromoted,d.LoadFile,d.DataType
   FROM EnergySTG.dbo.PromotionMaster p
   RIGHT JOIN (            SELECT DISTINCT LoadFile, 'OneLine' AS DataType FROM EnergyDB.dbo.OneLineData
               UNION ALL
                           SELECT DISTINCT LoadFile, 'Monthly' AS DataType FROM EnergyDB.dbo.MonthlyData
               ) d
                  ON LEFT(D.Loadfile,17)=LEFT(p.Filename,17)
                  AND p.DataType=d.datatype
   WHERE  (p.waspromoted=0 OR p.waspromoted is null)
)
SELECT b.LoadFile
FROM BASE b 
LEFT JOIN (SELECT FIlename,datatype, CASE WHEN p.wasPromoted = 1 THEN 1 ELSE 0 END waspromoted FROM EnergySTG.dbo.PromotionMaster p) p
   ON LEFT(b.Loadfile,17)=LEFT(p.Filename,17)
   AND p.DataType=b.datatype
GROUP BY b.LoadFile
HAVING MAX(CASE WHEN p.wasPromoted = 1 THEN 1 ELSE 0 END)=MIN(CASE WHEN p.wasPromoted = 1 THEN 1 ELSE 0 END)

/*Then Check for Files where WasPromoted=0 in EnergyDB
BASE Gets everything in EnergyDB that could join based on first 17 characters to PromotionMaster where waspromoted=1
That retrieves false flags where CustName was changed and a new entry was made and used for promotion but old entry was not removed
Remove those in the bottom query referencing BASE
(there will be 2 rows for those false flags: 1 where waspromoted=1 and one where waspromoted=0)
If Min(waspromoted)=Max(waspromoted) Then there's no false flag. Only the waspromoted=0 will show up*/

;
