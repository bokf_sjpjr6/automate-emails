/*
Save Here: \\netapp1\energyengineering\Energy DataMart\Customer Numbers
As: Customer_Numbers_YYYYMMDD.xlsx
*/

/* Kicks out LOC lines by logic: anything with 2 lines on the CustNum and TotalCommitedAmount < 1 million */
/* SEE TXT FILE: "CPDM - BDWEnergyProductionLoanData - Notes.txt"*/
/*Use BTCBDXDBP001P.ERMA */

SET NOCOUNT ON;

/*VARIABLE FOR EFFECTIVE DATE*/
DECLARE @EFFECTIVE_DATE AS FLOAT
/*SET @EFFECTIVE_DATE = 202202;*/ /* YYYYMM format*/
SET @EFFECTIVE_DATE = $(VAR1); /* YYYYMM format*/

/**/
WITH BASE_DATA AS
		(
				SELECT	ROW_NUMBER() OVER(PARTITION BY ccf.BalDateID, cd.CustNum, i.MstrAcctNum ORDER BY ccf.CommittedAmt DESC) AS RecordID /*Needed to identify master accounts*/
						, ccf.BalDateID
						, cd.CustNum
						, i.MstrAcctNum
						, ccd.AcctName
						, opd.OfficerName + ' - ' + opd.OfficerNum AS OfficerFullName
						, cost.CostCtrNum
						, cost.LOB
						, SUM(ccf.CommittedAmt) OVER(PARTITION BY ccf.BalDateID, cd.CustNum, i.MstrAcctNum) AS TotalCmtAmount
						, SUM(ccf.OutstandingAmt) OVER(PARTITION BY ccf.BalDateID, cd.CustNum, i.MstrAcctNum) AS TotalOSAmount
						, SUM(ccf.CommittedAmt * CAST(rgd.RiskGrade AS int)) OVER(PARTITION BY ccf.BalDateID, cd.CustNum, i.MstrAcctNum) AS TotalWARGAmount
						, SUM(ccf.TotalDealAmount) OVER(PARTITION BY ccf.BalDateID, cd.CustNum, i.MstrAcctNum) AS TotalDealAmount

				FROM ERMA.dbo.FMCustomerCreditFact AS ccf
					JOIN ERMA.dbo.BorrowerCodeDim AS bcd			ON bcd.BorrowerCodeDimID = ccf.BorrowerCodeDimID /*Regulatory reporting codes*/
					JOIN ERMA.dbo.CompanyDim AS comp				ON ccf.CompanyDimID = comp.CompanyDimID	/*Identify which "bank" owns the loan*/
					JOIN ERMA.dbo.CostCtrDim AS cost				ON ccf.CostCtrDimID = cost.CostCtrDimID	/*Cost center hierarchy*/
					JOIN ERMA.dbo.CustomerDim AS cd					ON ccf.CustDimID = CD.CustDimID	/*Customer specific info */
					JOIN ERMA.dbo.FMClassCodeCurrentHierDim AS cch	ON ccf.ClassCodeDimID = cch.ClassCodeDimID /*Internal industry hierarchy*/
					JOIN ERMA.dbo.FMCustomerCreditDim AS ccd		ON ccf.CustCreditDimID = ccd.CustCreditDimID /*Customer loan info */
					JOIN ERMA.dbo.InstrumentDim AS i				ON ccf.InstrumentDimID = i.InstrumentDimID /*Product type info*/
					JOIN ERMA.dbo.InstrumentTypeDim AS it			ON ccf.InstrumentTypeDimID = it.InstrumentTypeDimID	/*Product type dim*/
					JOIN ERMA.dbo.OfficerPeopleDim AS opd			ON ccf.OfficerPeopleDimID = opd.OfficerPeopleDimID /*Officer info*/
					JOIN ERMA.dbo.PastDueBandDim AS pd				ON ccf.PastDueBandDimID = pd.PastDueBandDimID /*Loan performance info*/
					JOIN ERMA.dbo.RiskGradeDim AS rgd				ON ccf.RiskGradeDimID = rgd.RiskGradeDimID /*Risk grade info*/
					JOIN ERMA.dbo.TimeDim AS t						ON ccf.BalDateID = t.TimeDimID /*lots of helpful time-related attributes*/

				WHERE t.YearMonthNum = @EFFECTIVE_DATE /*@EFFECTIVE_DATE USED HERE*/
					AND it.GradedNonGraded = 'Graded'
					AND cch.ClassCodeNumeric IN (30) /*30 = Energy Production*/
					AND rgd.RiskGradeDimID NOT IN (-1, -2)	/*Excludes NF and NA records*/
		)
, INTM AS (
SELECT	/*BalDateID
		, */CustNum
		, MstrAcctNum
		, AcctName
		, OfficerFullName
		, CostCtrNum
		, LOB
		, TotalCmtAmount
		, TotalOSAmount
		, CAST(TotalWARGAmount / TotalCmtAmount AS DECIMAL(4,2)) AS RiskGrade
		, TotalDealAmount

FROM BASE_DATA

WHERE	1=1
		AND RecordID = 1
		AND TotalCmtAmount <> 0
		AND LOB IN ('Energy Lending', 'Special Assets')	/*This excludes PPP, private wealth, and commercial loans that coded as E&P*/
)
, Find_Multi_Liners AS (
SELECT CustNum
		, MstrAcctNum
		, AcctName
		, OfficerFullName
		, CostCtrNum
		, LOB
		, TotalCmtAmount
		, TotalOSAmount
		, RiskGrade
		, TotalDealAmount
, case when count(*) over (partition by CustNum) =2 THEN 2 ELSE 1 END multi_line
FROM INTM
GROUP BY CustNum
		, MstrAcctNum
		, AcctName
		, OfficerFullName
		, CostCtrNum
		, LOB
		, TotalCmtAmount
		, TotalOSAmount
		, RiskGrade
		, TotalDealAmount
      )

SELECT CustNum
		, MstrAcctNum
		, AcctName
		, OfficerFullName
		, CostCtrNum
		, LOB
		, TotalCmtAmount
		, TotalOSAmount
		, RiskGrade
		, TotalDealAmount
FROM Find_Multi_Liners
WHERE multi_line=1 or (multi_line=2 AND TotalCmtAmount>1000000) /*kick out small LOC lines*/
