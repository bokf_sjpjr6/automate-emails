/*Check for Files where WasPromoted=1 but not in EnergyDB*/
/*In TheStart, Filter promotionMaster to only get files within past 13 months
In TheMiddle & The Middle2, get to only the latest file for each customer
In the final select find matches to XXXXXXXX_Y_PPPPP in EnergyDB*/
/*Uses "StringSplit" Function*/

WITH TheStart AS (
   SELECT
      filename
      , EnergySTG.dbo.StringSplit (filename,'_',3) PriceDeck
      , EnergySTG.dbo.StringSplit (filename,'_',1) CustNum
   FROM EnergySTG.dbo.promotionMaster
   WHERE PromotionDate >= DATEADD(MONTH, -13, GETDATE())
)

,TheMiddle AS (
   SELECT
      CustNum
      , FIRST_VALUE(filename) OVER (partition by CustNum ORDER BY (CONCAT(substring(PriceDeck,3,2),substring(PriceDeck,1,2),'0',substring(PriceDeck,5,1))) DESC) filename
      , FIRST_VALUE(PriceDeck) OVER (partition by CustNum ORDER BY (CONCAT(substring(PriceDeck,3,2),substring(PriceDeck,1,2),'0',substring(PriceDeck,5,1))) DESC) PriceDeck_Sortable
   FROM TheStart
)

SELECT DISTINCT p.*,o.LoadFile
FROM EnergySTG.dbo.PromotionMaster p
LEFT JOIN (SELECT DISTINCT LoadFile FROM EnergyDB.dbo.OneLineData) o ON LEFT(o.Loadfile,17)=LEFT(p.Filename,17) AND p.DataType='OneLine'
LEFT JOIN (SELECT DISTINCT LoadFile FROM EnergyDB.dbo.MonthlyData) m ON LEFT(m.Loadfile,17)=LEFT(p.Filename,17) AND p.DataType='Monthly'
JOIN TheMiddle m2 on m2.[filename]=p.[FileName]
WHERE ((o.LoadFile IS NULL AND DataType='OneLine') OR (m.LoadFile IS NULL  AND DataType='Monthly'))
AND p.waspromoted=1
AND promotionDate>DATEADD(month,-6,getdate())
;
