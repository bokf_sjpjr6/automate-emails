# Get Current Date in Different Forms to be Used Later
$date_today_yyyymmdd = Get-Date -Format "yyyyMMdd"
$date_lastmonth_yyyymm = (get-date).AddMonths(-1).ToString("yyyyMM") 

# Current Filepath
$filepath_home = Get-Location
$filepath_home=$filepath_home.Path
# Filepath of SQL Script
$FilePath_SQL = "$filepath_home\Scripts\Get Cust Num List without LOC.sql"
# SQL Parameters
$SQLServer="AGBDWPRDLSTNER"
$myconectionstring="server='$SQLServer';trusted_connection='true'; integrated security='true'; multiSubnetFailover='true'; applicationIntent='ReadOnly'"
# Run SQL Query
write-host "Beginning SQL Query"
$Datatable=Invoke-sqlcmd -InputFile $FilePath_SQL -connectionstring $myconectionstring -OutputAs DataTables -Variable "VAR1='$date_lastmonth_yyyymm'"
write-host "Finished SQL Query"

# \\netapp1\energyengineering\Energy DataMart\Customer Numbers
$outputpath="\\netapp1\energyengineering\Energy DataMart\Customer Numbers\Customer_Numbers_$date_today_yyyymmdd.xlsx"

Install-Module -Name ImportExcel -Scope CurrentUser
Write-Host 'Begin Putting Row Data in Excel...'
# Export to Excel without Excel, exclude unwanted system columns
# http://www.martinguth.de/reporting/exporting-to-excel-from-powershell-without-having-excel-installed/
$Datatable | Select-Object -Property * -ExcludeProperty RowError, RowState, Table, ItemArray, HasErrors | Export-Excel $outputpath
Write-Host 'Finished Putting Row Data in Excel.'
