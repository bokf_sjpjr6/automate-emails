# Set up API Request for Token
$clientId='18524-direct-access'
$clientSecret='427d536a-335c-4af0-845e-711b911b69b6'
$APIKey='b71313ab8dede14efb1d50f90725bfc3'

$Text = $clientId+":"+$clientSecret
$Bytes = [System.Text.Encoding]::UTF8.GetBytes($Text)
$EncodedText =[Convert]::ToBase64String($Bytes)
$EncodedText

$headers_TokenRequest = @{
    'Content-Type' = 'application/x-www-form-urlencoded'
    'X-API-KEY' = $APIKey
    'Authorization' = 'Basic '+$EncodedText
}

# Make Request for Token
Write-Host 'Make Request for Token'
$url_TokenRequst='https://di-api.drillinginfo.com/v2/direct-access/tokens?grant_type=client_credentials'
$TokenRequestResponse=Invoke-WebRequest -method 'Post' -uri $url_TokenRequst -Headers $headers_TokenRequest
$StatusCode=$TokenRequestResponse.StatusCode
Write-Host "Response Code: $StatusCode"
$myToken=$TokenRequestResponse.Content.Split('"')[3]


# Set up request for Company Acreage (PE Sponsor)
Write-Host 'Set up request for Company Acreage (PE Sponsor)'
$baseURL= "https://di-api.drillinginfo.com/v2/direct-access"
$url=$baseURL+'/company-acreage'
$pagesize=100000 # 100,000 is max allowed by Enverus. Should be plenty for all of company-acreage table
$url_PESponsor=$url+'?pagesize='+$pagesize

$headers_PESponsor = @{
    'X-API-KEY' = $APIKey
    'Authorization' = "Bearer $myToken"
}
# Make request for Company Acreage (PE Sponsor)
Write-Host 'Make request for Company Acreage (PE Sponsor)'
$response_PESponsor=Invoke-RestMethod -method 'Get' -Uri $url_PESponsor -Headers $headers_PESponsor
Write-Host 'Finished request for Company Acreage (PE Sponsor)'

# Convert Response To Datatable
$filepath_home = Get-Location
$filepath_home=$filepath_home.Path
. "$filepath_home\Functions\Object To Datatable Function.ps1"
$MyDataTable=ConvertTo-DataTable $response_PESponsor

# Do SQL
Import-Module DBATools
Write-Host "Drop SQL Table lu_PESponsor if Exists"
Invoke-DbaQuery -SqlInstance "VRT-SQL-017-WDP" -Query "DROP TABLE IF EXISTS EnergySTG.dbo.lu_PESponsor"
# Having the function automatically make the SQL table bc columns can possibly be in different order
Write-Host "Write Data to SQL Table lu_PESponsor"
Write-DbaDbTableData -SqlInstance 'VRT-SQL-017-WDP' -InputObject $MyDataTable -Table lu_PESponsor -Database EnergyStg -AutoCreateTable

# Remove where PESponsor is NULL or length of 0
Write-Host "Delete Rows from SQL table Where PESponsor is Null or length=0"
# Invoke-DbaQuery -SqlInstance "VRT-SQL-017-WDP" -Query "DELETE FROM EnergySTG.dbo.lu_PESponsor WHERE len(PESponsors) = 0 OR PESponsors is null"

# Fix types (necessary bc Write-DbaDbTableData's automatically chosen ones aren't great)
$alter_table_script=Get-Content -Path ".\Scripts\Alter Types lu_PESponsor Table.sql"
# Get-Content gives an array which can't be passed to Invoke-DbaQuery, so join it to be a string
$alter_table_script=$alter_table_script -join ''
Write-Host "Alter Data Types of SQL Table"
Invoke-DbaQuery -SqlInstance "VRT-SQL-017-WDP" -Query $alter_table_script
