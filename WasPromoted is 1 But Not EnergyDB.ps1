﻿# Current Filepath
$filepath_home = Get-Location
$filepath_home=$filepath_home.Path

# Sql Serven and Connection String
$SQLServer="VRT-SQL-017-WDP"
$myconectionstring="server='$SQLServer';database='EnergySTG';trusted_connection=false; integrated security='true'; MultipleActiveResultSets='true'"
# Drop function query
$query_dropfunction="DROP FUNCTION IF EXISTS dbo.StringSplit"
import-module SqlServer
write-host "Drop SQL Function"
Invoke-sqlcmd -connectionstring $myconectionstring -Query $query_dropfunction
# Create function query
$filepath_query_createfunction="$filepath_home\Scripts\Create SQL StringSplit Function.sql"
write-host "Create SQL Function"
Invoke-sqlcmd -InputFile $filepath_query_createfunction -connectionstring $myconectionstring 

# Run Main SQL to get data into Table
echo "Querying for WasPromoted is 1 But Not EnergyDB"
$filepath_query_mainquery = "$filepath_home\Scripts\WasPromoted is 1 But Not EnergyDB.sql"
write-host "Beginning SQL Query"
$Datatable=Invoke-sqlcmd -InputFile $filepath_query_mainquery -connectionstring $myconectionstring -OutputAs DataTables
write-host "Finished SQL Query"

# Function to Convert Table to HTML
# https://sqljunkieshare.com/2015/12/25/convert-data-table-to-html-table-with-powershell-and-more/
. "$filepath_home\Functions\Convert DataTable to HTML Function.ps1"

# Run Function to Convert Data Table to HTML
$Datatable_HTML = Convert-DatatablHtml -dt $Datatable

# Send Mail IF there are > 0 Rows of table
write-host "Preparing Email"
if ($Datatable.Rows.Count -gt 0) {
    $Outlook = New-Object -ComObject Outlook.Application
    $Mail = $Outlook.CreateItem(0)
    $Mail.To = "spatterson@bokf.com;EnergyAnalytics@bokf.com"
    $Mail.Subject = "WasPromoted is 1 But Not EnergyDB"
    $Mail.HTMLBody = $Datatable_HTML
    $Mail.Send()
}